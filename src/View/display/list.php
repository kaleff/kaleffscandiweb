<link rel="stylesheet" href="../../../media/css/list.css">
<header>
    <div class="row">
        <div class="col-md-6">
            <h1 id="pAdd">Product List</h1>
        </div>
        <div class="col-md-6 mainButtons">
            <form action="/add-product" id="addProd" method="post">
                <input type="submit" value="ADD"
                form="addProd" class="btn btn-secondary" id="addProductButton">
            <input type="submit" value="MASS DELETE" 
            form="checkboxes" class="btn btn-secondary" id="delete-product-btn">
            </form>
        </div>
    </div>
    <hr>
</header>
<body>
    <div class = "article">
        <form id="checkboxes" action="/delete" method="post">
        </form>
        <?php foreach($this->data as $dataInstance) {
            ?>
        <div class="articles">
            <input class="delete-checkbox" form="checkboxes" type="checkbox" 
            name="id<?php echo $dataInstance[0];?>" 
                value="<?php echo $dataInstance[0]; ?>">
            <p><?php echo $dataInstance[1]; ?></p>
            <p><?php echo $dataInstance[3]; ?></p>
            <p><?php echo $dataInstance[4]. " $"; ?></p>
            <p><?php echo $dataInstance[5]; ?></p>
        </div>
        <?php } ?>
    </div>
</body>
