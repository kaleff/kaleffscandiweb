<script src="../../../media/script/add.js"></script>
<link rel="stylesheet" href="../../../media/css/add.css">
<header>
    <div class="row">
        <div class="col-md-6">
            <h1 id="pAdd">Product Add</h1>
        </div>
        <div class="col-md-6">
            <form action="/" id="cancelProd" method="post">
                <input type="submit" value="Cancel"
                form="cancelProd" class="btn btn-secondary" id="cancelProductButton">
            <input type="submit" value="Save" form="product_form" 
            class="btn btn-secondary" id="addButton">
            </form>
        </div>
    </div>
</header>
<hr>
<?php 
if(gettype($data) == "boolean") {
    echo ($data) ? '<p class ="warning">This SKU code already exists!</p>' : "";
}
?>
<form id="product_form" action="/add-product" method="post">
    <label for="sku">SKU Code: </label>
    <input type="text" id="sku" name="SKU" 
    required="required" autofocus class="inputForm"><br>
    <label for="name">Name: </label>
    <input type="text" id="name" name="pTitle" 
    required="required" class="inputForm"><br>
    <label for="price">Price: </label>
    <input type="number" min="0.01" max="1000000" step="0.01" value="0.00" 
    id="price" name="pPrice" required="required" class="inputForm"><br>
    <label for="pType">Type: </label>
    <select onchange="FormChange(this.value)" class="inputForm"
    id="productType" name="pType" required="required" id="productType">
        <option value="NoValue"></option>
        <option value="Book">Book</option>
        <option value="Dvd">DVD</option>
        <option value="Furniture">Furniture</option>
    </select>
    <p id="Dummy">Please choose the type of product</p>
    <div id="theBlock"></div>
</form>
<?php
if($data === "success") {
    echo "<p>Product Added!</p>".
    '<form action="/">
        <input class="btn btn-secondary" type="submit" value="Go to Product List" />
    </form>';
}
