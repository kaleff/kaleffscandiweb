<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product Management</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0"
          crossorigin="anonymous"
    >
</head>
<body>
<?php
require_once ROOT . "/View/" . $this->path . ".php";
?>
<footer>
    <hr>
    <h3 style="text-align: center">Scandiweb Test assignment</h3>
</footer>
</body>
</html>
