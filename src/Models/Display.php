<?php

namespace App\Models;

class Display extends Model
{
    public function deleteArticles()
    {
        $arrayId = [];
        foreach($_POST as $articleId=>$artId) {
            $arrayId[] = $artId;
        }
        $arrayId = implode(", ", $arrayId);
        $deleteItemQuery = mysqli_query(
            $this->connection, "DELETE FROM `products`
                                WHERE `id` 
                                IN ($arrayId)"
        );
        $host  = $_SERVER['HTTP_HOST'];
        header("Location: http://$host/");
        exit;
    }
}
