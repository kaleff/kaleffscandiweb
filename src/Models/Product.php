<?php

namespace App\Models;

class Product extends Model
{
    public function checkSku($sku)
    {
        $skuQuery = mysqli_query(
            $this->connection, 
            "SELECT `SKU_id` FROM `products`"
        );
        $skuList = mysqli_fetch_all($skuQuery);
        for($x = 0; $x<=count($skuList)-1; $x++) {
            $skuList[$x] = $skuList[$x][0];
        }
        foreach($skuList as $skuSample) {
            if($sku == $skuSample) {
                return true;
            }
        }
        return false;
    }
    public function addNewItem($newItem)
    {
        $itemType = $newItem['pType'];
        $itemType = ucfirst($itemType);
        $itemFunc = "add".$itemType;
        $specs = $this->$itemFunc($newItem);
        $statement = "'".$newItem['SKU']."', '".$newItem['pType']."', '".
        $newItem['pTitle']."', '".$newItem['pPrice']."', '".$specs."'";
        $addItemQuery = mysqli_query(
            $this->connection, "INSERT INTO `products` 
                                                                (`id`, `SKU_id`, 
                                                                `type`, `title`, 
                                                                `price`, `specs`)
                                                        VALUES (NULL, $statement)"
        );
    }
    protected function addDvd($newItem)
    {
        return "Size: ".$newItem['size']." mb";
    }
    protected function addBook($newItem)
    {
        return "Weight: ".$newItem['weight']." kg";
    }
    protected function addFurniture($newItem)
    {
        return "Dimensions: ".$newItem['height']."x".
        $newItem['width']."x".$newItem['length'];
    }
}
