<?php

namespace App\Controllers;

class MainController extends Controller
{
    public function indexAction()
    {
        $this->view->render(null);
    }
}
