<?php

namespace App\Controllers;

class ProductController extends Controller
{
    public $newProduct;
    public function addAction()
    {
        if(isset($_POST['SKU'])) {
            if($this->model->checkSku($_POST['SKU'])) {
                $this->newProduct = $this->view->render(true);
            }
            else {
                $this->newProduct = $this->view->render("success");
                $this->model->addNewItem($_POST);
            }
        }
        else {
            $this->newProduct = $this->view->render(false);
        }
    }
}
