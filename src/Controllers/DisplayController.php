<?php

namespace App\Controllers;

class DisplayController extends Controller
{
    public function listAction()
    {
        $productList = $this->model->getDatabaseInfo();
        $this->view->render($productList);
    }
    public function deleteAction()
    {
        $this->model->deleteArticles();
    }
}
