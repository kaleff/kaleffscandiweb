<?php

return array(
    "add-product" => [
        "controller" => "product",
        "action" => "add"
    ],
    "delete" => [
        "controller" => "display",
        "action" => "delete"
    ],
    "main" => [
        "controller" => "main",
        "action" => "index"
    ],
    "" => [
        "controller" => "display",
        "action" => "list"
    ]
);
