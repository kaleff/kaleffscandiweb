I've done the task with the help of custom MVC-framework built from scratch.
No other frameworks were used.
All the PSR standards are complied with.

Import database from database/directory
If needed, change Database connections parameters at src/config/db_parameters.php

Hosted version - http://kaleffscandiweb.tk

Contact - markskalijevs@gmail.com

Note: Instead of redirecting to the product list, it instead creates the button link to the product list instead, since HEADER method conflicts with the way that webpage is built.
I hope for your understanding.