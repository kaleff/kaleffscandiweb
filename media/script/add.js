const DVD = {
    'type': 'number',
    'min' : "0.1",
    'max' : '20000',
    'step' : '0.1',
    'value' : '0.00',
    'name' : 'size',
    'id' : 'size',
    'class' : "inputForm",
    'required' : 'required'
};
const BOOK = {
    "type": "number",
    "name": "weight",
    "id": "weight",
    "min": "0.001",
    "max": "1000",
    "step": "0.001",
    "value": "0.000",
    'class' : "inputForm",
    "required": "required"
};
const FURNITURE = {
    "type": "number",
    "required": "required",
    "min": "1",
    "max": "10000",
    "value": "0.0",
    'class' : "inputForm",
    "step": "1"
};

class Item {
    constructor() {
        let label = document.createElement("label");
        label.setAttribute("for", "opt");
        label.setAttribute("id", "optLabel");
        document.getElementById('theBlock').appendChild(label);
    }
}
class Dvd extends Item {
    build() {
        document.getElementById("optLabel").innerHTML = "Please select the DVD size (in MB): ";
        let dvdInput = document.createElement("input");
        for (const [attr, value] of Object.entries(DVD)) {
            dvdInput.setAttribute(attr, value);
        }
        document.getElementById('theBlock').appendChild(dvdInput);
    }
}
class Book extends Item {
    build() {
        document.getElementById("optLabel").innerHTML = "Please select the book weight (in Kg): ";
        let bookInput = document.createElement("input");
        for (const [attr, value] of Object.entries(BOOK)) {
            bookInput.setAttribute(attr, value);
        }
        document.getElementById('theBlock').appendChild(bookInput);
    }
}
class Furniture extends Item {
    build() {
        let br = document.createElement("br");
        document.getElementById("optLabel").innerHTML = "Please enter the furniture Height (cm): ";
        let furnitureInput1 = document.createElement("input");
        furnitureInput1.setAttribute("name", "height");
        furnitureInput1.setAttribute("id", "height");
        for (const [attr, value] of Object.entries(FURNITURE)) {
            furnitureInput1.setAttribute(attr, value);
        }
        document.getElementById("theBlock").appendChild(furnitureInput1);
        document.getElementById("theBlock").appendChild(br.cloneNode());

        let label2 = document.createElement("label");
        label2.setAttribute("for", "opt2");
        label2.setAttribute("id", "optLabel2");
        document.getElementById("theBlock").appendChild(label2);

        document.getElementById("optLabel2").innerHTML = "Please enter the furniture Width (cm): ";
        let furnitureInput2 = document.createElement("input");
        furnitureInput2.setAttribute("name", "width");
        furnitureInput2.setAttribute("id", "width");
        for (const [attr, value] of Object.entries(FURNITURE)) {
            furnitureInput2.setAttribute(attr, value);
        }
        document.getElementById("theBlock").appendChild(furnitureInput2);
        document.getElementById("theBlock").appendChild(br.cloneNode());
        
        let label3 = document.createElement("label");
        label3.setAttribute("for", "opt3");
        label3.setAttribute("id", "optLabel3");
        document.getElementById("theBlock").appendChild(label3);

        document.getElementById("optLabel3").innerHTML = "Please enter the furniture Length (cm): ";
        let furnitureInput3 = document.createElement("input");
        for (const [attr, value] of Object.entries(FURNITURE)) {
            furnitureInput3.setAttribute(attr, value);
        }
        furnitureInput3.setAttribute("name", "length");
        furnitureInput3.setAttribute("id", "length");
        document.getElementById("theBlock").appendChild(furnitureInput3);
        document.getElementById("theBlock").appendChild(br.cloneNode());
    }
}
class NoValue {
    build() {
        let dummy = document.createElement("p");
        dummy.setAttribute("id", "Dummy");
        document.getElementById('Dummy').innerHTML = "Please choose the type of product";
        document.getElementById("theBlock").appendChild(dummy);
        document.getElementById("product_form").appendChild(theBlock);
    }
}
function FormChange(value)
{
    document.getElementById('theBlock').innerHTML = "";
    document.getElementById("Dummy").innerHTML = "";
    console.log(value);
    eval( "var newForm = new " + value + "();");
    newForm.build();
}